using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.ProBuilder;

public class InventoryHandler : MonoBehaviour
{

   
    // Start is called before the first frame update
    [SerializeField] public  List<RenderPosition> inventorySlots = new List<RenderPosition>();
    [SerializeField] private GameObject invParent;
    private bool inventoryFull;

  public int itemsCounter()
    {
        return CountItems();
    }
    private Vector3 currentFreeRenderPosition;
    public GameObject[] renderPositions;


   public enum InventoryState
    {
        SameItemAddedSuccess,
        NewSlotCreatedSuccess,
        NotEnoughSpace
    }

    public static InventoryState invState;
    void Start()
    {


        for (int i = 0; i < 3; i++)
        {
            SlotReset(i);
        }
      
    }

    // Update is called once per frame
    void Update()
    {

 
    }


  

  public  void ShiftSlotInfo()
    {
        for (int i = 1; i < 3; i++)
        {
            if (inventorySlots[i].slotData.isSlotUsed && !inventorySlots[i - 1].slotData.isSlotUsed)
            {
                Debug.Log("YASSSS");
                inventorySlots[i].slotData.isSlotUsed = false;
                inventorySlots[i - 1].slotData.isSlotUsed = true;
                inventorySlots[i - 1].slotData.itemQuantity = inventorySlots[i].slotData.itemQuantity;
                inventorySlots[i - 1].slotData.itemMaxQuantity = inventorySlots[i].slotData.itemMaxQuantity;
                inventorySlots[i - 1].slotData.itemID = inventorySlots[i].slotData.itemID;
                inventorySlots[i - 1].slotData.itemName = inventorySlots[i].slotData.itemName;
                inventorySlots[i - 1].attachedGameobject = inventorySlots[i].attachedGameobject;
                SlotReset(i);
            }
        }
    }

    public int HolesAmount()
    {
        int holes = 0;
       
        for (int i = 1; i < 3; i++)
        {
            if (inventorySlots[i].slotData.isSlotUsed && !inventorySlots[i - 1].slotData.isSlotUsed)
            {
               
                holes++;
            }
        }

        return holes;
    }
   

  public  InventoryState AddItem(Loot passedItem,GameObject item)
  {
      if (passedItem.lootData.quantity > 0)
      {
          foreach (var slot in inventorySlots)
          {
              if (slot.slotData.itemID == passedItem.lootData.id)
              {
                
                  // Проверяем, можно ли увеличить количество
                  if (slot.slotData.itemQuantity < slot.slotData.itemMaxQuantity)
                  {
                  
                      int availableSpace =slot.slotData.itemMaxQuantity - slot.slotData.itemQuantity;
                      int amountToTake = Mathf.Clamp(passedItem.lootData.quantity, 0, availableSpace); // Используем Mathf.Clamp
                      slot.slotData.itemQuantity += amountToTake;
                      passedItem.lootData.quantity -= amountToTake;

                      // Если весь переданный предмет помещен, возвращаем true
                      if (passedItem.lootData.quantity == 0)
                      {
                          return  InventoryState.SameItemAddedSuccess;
                      }
                  }
              }
          }
      

          // Если предмета нет в списке или он не помещается в существующий слот
          if (!inventoryFull && passedItem.lootData.quantity > 0)
          {
     
              RenderPosition   temp = GetInventoryFreeSlot();
      
              temp.slotData.itemID = passedItem.lootData.id;
              temp.slotData.itemQuantity = passedItem.lootData.quantity;
              temp.slotData.itemMaxQuantity = passedItem.lootData.maxQuantity;
              temp.slotData.itemName = passedItem.lootData.name;
              temp.attachedGameobject = item;
              passedItem.lootData.currentSlot = temp.slotData;
              return InventoryState.NewSlotCreatedSuccess;
          }

          // Если инвентарь полный или предмет не помещается полностью
          
      }
      Debug.Log("Inventory is full");
      return InventoryState.NotEnoughSpace;
          
    }


    public int CountItems()
    {
       
        int temp;
        temp = 0;
        foreach (var VARIABLE in inventorySlots)
        {
            if (VARIABLE.slotData.itemID != 0)
            {
                temp++;
            }
        }

        return temp;

    }

    RenderPosition GetInventoryFreeSlot()
    {
        foreach (var slot in inventorySlots)
        {
            if (slot.slotData.isSlotUsed == false)
            {
              Debug.Log("SCERX");
           slot.slotData.isSlotUsed = true;
                return slot;
                
                
            }
        }
    
        return null;
    }
    

  public  void SlotReset(int i)
    {
        inventorySlots[i].slotData.isSlotUsed = false;
        inventorySlots[i].slotData.itemQuantity = 0;
       inventorySlots[i].slotData.itemMaxQuantity = 0;
       inventorySlots[i].slotData.itemName = "";
        inventorySlots[i].slotData.itemID = 0;
        inventorySlots[i].attachedGameobject = null;
    }
  
}


  
