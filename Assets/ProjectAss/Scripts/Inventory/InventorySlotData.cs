using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "InventorySlot",fileName = "Slot")]
public class InventorySlotData : ScriptableObject
{
    public string itemName;
    public int itemID;
    public int itemQuantity;
    public int itemMaxQuantity;
    public bool isSlotUsed;
    public Transform itemTransform;

}
