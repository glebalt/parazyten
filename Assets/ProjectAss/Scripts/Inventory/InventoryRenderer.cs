using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class InventoryRenderer : MonoBehaviour
{

    [SerializeField] private GameObject playerCamHolder;
   private Vector3 currentFreePos;
 private InventoryHandler refInvHandler;
   [SerializeField] private GameObject invCam;
   [SerializeField] private GameObject invLight;

   [SerializeField] private TextMeshProUGUI itemText;
   [SerializeField] private TextMeshProUGUI itemQuantityText;
   [SerializeField] private CameraShaker inventoryCam;
   
   private int currentCamPos;


   [Header("Audio")] private AudioSource invAudio;

   [SerializeField] private AudioClip swapItemsClip1;
   [SerializeField] private AudioClip swapItemsClip2;
    // Start is called before the first frame update
    void Start()
    {
        currentCamPos = 0;
        refInvHandler = transform.gameObject.GetComponent<InventoryHandler>();
        invAudio = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        itemText.text = refInvHandler.inventorySlots[currentCamPos].slotData.itemName;
        itemQuantityText.text = refInvHandler.inventorySlots[currentCamPos].slotData.itemQuantity.ToString() + " / " +
                                refInvHandler.inventorySlots[currentCamPos].slotData.itemMaxQuantity.ToString();
        
        DropItem();
        CurrentCameraPos(SwitchItem());
  RenderItems();
     
 
    
    }

    public  void RenderItems()
    {
        int counter = 0;
        for (int i = 0; i < 3;i++)
        {
        
            if (refInvHandler.inventorySlots[i].attachedGameobject != null)
            {
                if (currentCamPos == i)
                {
                    Vector3 targetPos = new Vector3(
                        refInvHandler.renderPositions[counter].transform.position.x
                        ,  refInvHandler.renderPositions[counter].transform.position.y,
                        refInvHandler.renderPositions[counter].transform.position.z- 1);
                    refInvHandler.inventorySlots[i].attachedGameobject.transform.position = Vector3.Lerp
                    (refInvHandler.inventorySlots[i].attachedGameobject.transform.position, targetPos,
                        Time.deltaTime * 10);
                    refInvHandler.inventorySlots[i].attachedGameobject.transform.rotation = Quaternion.Euler(0,0,0);
                    counter++;
                }
                else
                {
                    refInvHandler.inventorySlots[i].attachedGameobject.transform.position = Vector3.Lerp(
                        refInvHandler.inventorySlots[i].attachedGameobject.transform.position
                        , refInvHandler.renderPositions[counter].transform.position, Time.deltaTime * 10);
                    refInvHandler.inventorySlots[i].attachedGameobject.transform.rotation = Quaternion.Euler(0,0,0);
                    counter++;
                }
              

            }
              
          
        }
    }







  

    int SwitchItem()
    {
        if (Input.GetKeyDown(KeyCode.A) && currentCamPos > 0 &&  refInvHandler.itemsCounter() > 1)
        {
            invAudio.clip = swapItemsClip1;
            invAudio.Play();
            inventoryCam.CallCamInvLeftSwitch();
            currentCamPos--;
            
         
        }
        if (Input.GetKeyDown(KeyCode.D) && currentCamPos < refInvHandler.itemsCounter() - 1)
        {
            invAudio.clip = swapItemsClip2;
            invAudio.Play();
            inventoryCam.CallCamInvRightSwitch();
            currentCamPos++;
           
         
        }

        currentCamPos = Math.Clamp(currentCamPos, 0, refInvHandler.CountItems());
        return currentCamPos;
    }

    void DropItem()
    {
      
        if (Input.GetKeyDown(KeyCode.Return))
        {
            inventoryCam.CallCamShakeMedium();
            if (currentCamPos == refInvHandler.CountItems() - 1)
            {
                refInvHandler.inventorySlots[currentCamPos].attachedGameobject.transform.position =
                    new Vector3(playerCamHolder.transform.position.x, playerCamHolder.transform.position.y, playerCamHolder.transform.position.z + 0.3f);
                
                refInvHandler.inventorySlots[currentCamPos].attachedGameobject.GetComponent<Loot>().lootData.quantity =
                    refInvHandler.inventorySlots[currentCamPos].slotData.itemQuantity;
                refInvHandler.inventorySlots[currentCamPos].attachedGameobject.GetComponent<Rigidbody>().isKinematic =
                    false;
                refInvHandler.inventorySlots[currentCamPos].slotData.isSlotUsed = false;
                refInvHandler.SlotReset(currentCamPos);
                currentCamPos--;
            }
            else
            {
                refInvHandler.inventorySlots[currentCamPos].attachedGameobject.transform.position =
                    new Vector3(playerCamHolder.transform.position.x, playerCamHolder.transform.position.y, playerCamHolder.transform.position.z + 0.3f);
                
                refInvHandler.inventorySlots[currentCamPos].attachedGameobject.GetComponent<Loot>().lootData.quantity =
                    refInvHandler.inventorySlots[currentCamPos].slotData.itemQuantity;
                refInvHandler.inventorySlots[currentCamPos].attachedGameobject.GetComponent<Rigidbody>().isKinematic =
                    false;
                refInvHandler.inventorySlots[currentCamPos].slotData.isSlotUsed = false;
                refInvHandler.ShiftSlotInfo();
            }
              
        
         
              
        }
      
     
    }

   

    void CurrentCameraPos(int itemIndex)
    {
       Vector3 targetCamPos = new Vector3(refInvHandler.renderPositions[itemIndex].transform.localPosition.x,
            refInvHandler.renderPositions[itemIndex].transform.localPosition.y + 0.45f, refInvHandler.renderPositions[itemIndex].transform.localPosition.z - 3f);

       invCam.transform.localPosition = Vector3.Lerp(invCam.transform.localPosition, targetCamPos, Time.deltaTime * 8);
       CurrentLightPos();
    }


    void CurrentLightPos()  
    {
        Vector3 targetLightPos = new Vector3( refInvHandler.renderPositions[currentCamPos].transform.localPosition.x, refInvHandler.renderPositions[currentCamPos].transform.localPosition.y + 2f,
            refInvHandler.renderPositions[currentCamPos].transform.localPosition.z- 1.6f);

        invLight.transform.localPosition =
            Vector3.Lerp(invLight.transform.localPosition, targetLightPos, Time.deltaTime * 8);
    }
 

  
    protected  bool CanHandleInventoryInput() => InteractionScript.currentInteractionState ==
                                                 InteractionScript.CurrentPlayerInteractionState.InInventory;



}
