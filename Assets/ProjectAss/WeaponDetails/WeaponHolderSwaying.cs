using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponHolderSwaying : MonoBehaviour
{
    private Quaternion targetRot;
    private Quaternion targetRot1;
    private Quaternion initRot;
    
    [Header("Rotation")]
    public int yRotLessThanZero;
    public int yRotGreaterThanZero;
    
    public int zRotLessThanZero;
    public int zRotGreaterThanZero;
    
    // Start is called before the first frame update
    void Start()
    {
        initRot = transform.localRotation;
    }

    // Update is called once per frame
    void Update()
    {
        
     Debug.Log(Input.GetAxis("Mouse X"));
        if (Input.GetAxis("Mouse X") > 0.2) 
        {
            targetRot = Quaternion.Euler(transform.localPosition.x,transform.localPosition.y +yRotGreaterThanZero,transform.localPosition.z + zRotGreaterThanZero);
            transform.localRotation = Quaternion.Slerp(transform.localRotation,targetRot,Time.deltaTime *3);
        }
        else if(Input.GetAxis("Mouse X") == 0)
        {
            transform.localRotation = Quaternion.Slerp(transform.localRotation,initRot,Time.deltaTime *5);
        }

        if (Input.GetAxis("Mouse X") < -0.2)
        {
            targetRot1 = Quaternion.Euler(transform.localPosition.x,transform.localPosition.y + yRotLessThanZero ,
                transform.localPosition.z + zRotLessThanZero);
            transform.localRotation = Quaternion.Slerp(transform.localRotation,targetRot1,Time.deltaTime *3);
        }
      
       
    }

 
}
