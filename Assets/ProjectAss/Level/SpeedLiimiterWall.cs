using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedLiimiterWall : MonoBehaviour
{

    [SerializeField] private PlayerControllee playerScriptRef;
    [SerializeField] private GameObject playerCamHolder;

    private BoxCollider col;
    // Start is called before the first frame update
    void Start()
    {
        int zex = 3;
       
        col = GetComponent<BoxCollider>();
        if (col == null)
        {
            Debug.Log($"Collider is null {zex}");
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            
          
            playerScriptRef.rb.AddForce(-playerCamHolder.transform.forward.normalized * 5,ForceMode.Force);
        }
    }
}
