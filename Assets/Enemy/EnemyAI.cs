using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;


public class EnemyAI : MonoBehaviour
{
    public static float playerHp;
    public float HP;
    [SerializeField] private TextMeshProUGUI hpRef;
    private float timeSinceLastAttack;
    //bools
    private bool playerInVisionRadius;
    private bool playerInAttackRadius;
    [SerializeField]  private bool hasWaypoints;
    
    //main
    private bool isAttacking;
    
    public GameObject[] waypoints;
    private int index;
    private NavMeshAgent agent;
    [SerializeField] private Animator animator;
    public GameObject Player;
    // Start is called before the first frame update

    [Header("Audio")] [SerializeField] private AudioClip[] clips;
    private AudioSource audio;
    void Start()
    {
        InvokeRepeating("PlayAudioStill",Random.Range(2,7),3f);
        isAttacking = false;
        HP = 100;
        index = 0;
        agent = GetComponent<NavMeshAgent>();
        audio = GetComponent<AudioSource>();
        playerHp = 100;

    }

    // Update is called once per frame
    void Update()
    {
      
        
        timeSinceLastAttack += Time.deltaTime;
      
        CheckStates();
        if (HP <= 0)
        {
            Destroy(gameObject);
        }

        if (playerInAttackRadius && timeSinceLastAttack > 2f)
        {
            StartCoroutine(DealDamage());
            animator.SetBool("Run",false);
            timeSinceLastAttack = 0;
            animator.SetTrigger("Attack");
            
   LookAtPlayer();
     }
        
        if (playerInVisionRadius && timeSinceLastAttack > 1f)
        {
            agent.isStopped = false;
            agent.SetDestination(Player.transform.position);
     animator.SetBool("Run",true);
          
      
        }


        if (playerHp < 1)
        {
            int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(currentSceneIndex);
        }
    
Debug.Log(!playerInVisionRadius && !playerInAttackRadius);
       
        if (!playerInVisionRadius && !playerInAttackRadius)
        {
           
            if (hasWaypoints)
            {
                animator.SetBool("Run",true);
                agent.SetDestination(waypoints[index].transform.position);
             
                if ( !agent.pathPending && agent.remainingDistance < agent.stoppingDistance)
                {
                    StartCoroutine(Wait());
                    index++;
                 
                    if (index > 3) index = 0;
                }
            }
            else
            {
                animator.SetBool("Run",false);
                animator.SetBool("Idle",true);
                agent.isStopped = true;
            }
            
        }
    }


    IEnumerator Wait()
    {
        
        animator.SetBool("Idle",true);
        float timer = 0;
        while (timer < 2)
        {
            timer += Time.fixedDeltaTime;
            agent.isStopped = true;
            yield return null;
        }
        agent.isStopped = false;
  
    }

    
    

    public void OnHit()
    {
        StartCoroutine(Wait());
    }

    void CheckStates()
    {
        playerInAttackRadius = Vector3.Distance(transform.position, Player.transform.position) < 3.4f;
        playerInVisionRadius = Vector3.Distance(transform.position, Player.transform.position) > 3.3f && Vector3.Distance(transform.position, Player.transform.position) < 60;
    }
    
    
    void LookAtPlayer()
    {
       
        transform.LookAt(Player.transform.position);

       
        Vector3 eulerRotation = transform.eulerAngles;
        eulerRotation.x = 0;
        eulerRotation.z = 0;
        transform.eulerAngles = eulerRotation;
    }

    void PlayAudioStill()
    {
        audio.clip = clips[Random.Range(0, 4)];
        audio.Play();
    }

    IEnumerator DealDamage()
    {
        yield return new WaitForSeconds(.5f);
        if (Vector3.Distance(transform.position, Player.transform.position) < 3.4f)
        {
            playerHp -= 10;
        }
        hpRef.text = playerHp.ToString();
        
    }
}
