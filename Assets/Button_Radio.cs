using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button_Radio : MonoBehaviour
{

 public enum GameStater
    {
        Started,
        Finished,
        Lost
    }
    
    public GameStater gameStater;

    [SerializeField]   private Camera mainCamera;
    public bool buttonPressed;
 
    private GameObject currentGmb;

    private Vector3 currentGmbInitPos;

    private Vector3 targetPos;


    [SerializeField] private AudioClip buttonClick;
    private AudioSource source;


    public DoorSplittedController doorReference;

    private void Start()
    {
        source = GetComponent<AudioSource>();
        source.clip = buttonClick;
        gameStater = GameStater.Lost;
    }


    void Update()
    {
      
        if (MiniGamePlayerStats_Radio1.playerHP < 1)
        {
            buttonPressed = false;
        }
        if (Input.GetKeyDown(KeyCode.Mouse0) && GlobalFuncs.isInInteractionMode )    
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.gameObject.CompareTag("Radio_Button"))
                {
                    currentGmb = hit.transform.gameObject;


                    StartCoroutine(PressButton());


                }
            }
        }

       
    }

    IEnumerator PressButton()
    {
        source.Play();
        buttonPressed = true;
        targetPos = new Vector3(currentGmb.transform.localPosition.x , currentGmb.transform.localPosition.y,
            currentGmb.transform.localPosition.z + 0.05f);
        float elaspedTime = 0.5f;
        float timer = 0;
        currentGmbInitPos = currentGmb.transform.localPosition;
        while (timer < elaspedTime)
        {
            timer += Time.deltaTime;
            currentGmb.transform.localPosition = Vector3.Lerp( currentGmb.transform.localPosition,targetPos
                ,Time.deltaTime * 8);
            yield return null;
        }
        
        targetPos = currentGmbInitPos;
        timer = 0;
        while (timer < elaspedTime)
        {
            timer += Time.deltaTime;
            currentGmb.transform.localPosition = Vector3.Lerp( currentGmb.transform.localPosition,
                targetPos,Time.deltaTime * 8);
            yield return null;
        }
        yield return new WaitForSeconds(.5f);
      
        buttonPressed = false;
    }
    
}
