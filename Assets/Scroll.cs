using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Scroll: MonoBehaviour
{
 [SerializeField]   protected Camera mainCamera;
 [SerializeField] private LayerMask scroolMask;
 
 
    private GameObject currentGmb;

    private Quaternion targetRot;

   private float rotX;

  public float GetRotX()
   {
       return rotX;
   }
    // Start is called before the first frame update
    void Start()
    {
     
        rotX = 0;
    }

    // Update is called once per frame
   protected void Update()
    {

        if (Input.GetKey(KeyCode.Mouse0) && GlobalFuncs.isInInteractionMode)    
        {
            // Создаем луч из позиции курсора мыши
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
        
            // Выполняем Raycast и проверяем попадание
            if (Physics.Raycast(ray, out hit))
            {
                // Проверяем тег объекта, на который попал луч
                if (hit.transform.gameObject.CompareTag("Radio_Scroll"))
                {
                    currentGmb = hit.transform.gameObject;
                   
                        rotX += 1;
                    
               
                    rotX = Mathf.Clamp(rotX, -70, 70);
                    targetRot = Quaternion.Euler(rotX,90,90);
                    currentGmb.transform.localRotation = Quaternion.Lerp(currentGmb.transform.localRotation,targetRot,Time.deltaTime * 2);
                }
          
                
            }
        }
        
        if (Input.GetKey(KeyCode.Mouse1) && GlobalFuncs.isInInteractionMode)    
        {
            // Создаем луч из позиции курсора мыши
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
        
            // Выполняем Raycast и проверяем попадание
            if (Physics.Raycast(ray, out hit))
            {
                // Проверяем тег объекта, на который попал луч
                if (hit.transform.gameObject.CompareTag("Radio_Scroll"))
                {
                    currentGmb = hit.transform.gameObject;
                
                    rotX -= 1;

                    rotX = Mathf.Clamp(rotX, -70, 70);
                    targetRot = Quaternion.Euler(rotX,90,90);
                    currentGmb.transform.localRotation = Quaternion.Lerp(currentGmb.transform.localRotation,targetRot,Time.deltaTime * 2);
                }
          
                
            }
        }
    }



}
