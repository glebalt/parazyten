using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorLogic : MonoBehaviour
{
    // Start is called before the first frame update

 [SerializeField]   private IsInDoorZone col;
 private bool DoorOpened;
 private Animator doorAnimator;
 private AudioSource src;
    void Start()
    {
        src = GetComponent<AudioSource>();
        DoorOpened = false;
        doorAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (col.IsInDoorArea)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                src.Play();
                ChangeState();
            }
        }
        if(DoorOpened)
        {
            doorAnimator.SetTrigger("Open");
        }

        if (DoorOpened = false)
        {
            doorAnimator.SetTrigger("Close");
        }
    }


    void ChangeState()
    {
        DoorOpened = !DoorOpened;
    }
}
