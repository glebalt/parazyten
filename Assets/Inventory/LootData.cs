using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Loot",fileName = "LootInstance")]
public class LootData : ScriptableObject
{

    public int id;
    public string name;
    public string description;
    public int quantity;
    public GameObject prefab;
    public int maxQuantity;
    public InventorySlotData currentSlot;
    // Start is called before the first frame update



}
