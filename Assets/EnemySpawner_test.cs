using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner_test : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform[] spawnPositions;
    [SerializeField] private GameObject playerPrefab;
    [SerializeField] private GameObject enemyPrefab;
    void Start()
    {
        InvokeRepeating("CallSpawner",0,30);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator enemySpaw()
    {
        for (int i = 0; i < 5; i++)
        {
            GameObject enemy = Instantiate(enemyPrefab, spawnPositions[Random.Range(0, 3)].position,Quaternion.Euler(0,0,0));
            EnemyAI enemyAI = enemy.GetComponent<EnemyAI>();
            enemyAI.Player = playerPrefab;
            Debug.Log("enemyspawn");
            yield return new WaitForSeconds(0.5f);
        }
    }

    void CallSpawner()
    {
        StartCoroutine(enemySpaw());
    }
}
