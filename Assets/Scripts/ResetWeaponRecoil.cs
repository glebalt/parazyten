using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetWeaponRecoil : MonoBehaviour
{
    private float recamountZRot;
    private float recamountXRot;
    private float recamountYRot;
    public AnimationCurve resetZCurve;

    public AnimationCurve resetXCurve;

    public AnimationCurve resetYCurve;


    private bool Activate;
    // Start is called before the first frame update
    void Start()
    {
        //Zrot
        float value;
        float time = 0;
        for (int i = 0; i < 4; i++)
        {
            if (i < 2)
            {
                if (i % 2 == 0)
                {
                    value = -30;
                }
                else
                {
                    value = 30;
                }

                Keyframe key = new Keyframe(time, value);
                resetZCurve.AddKey(key);
                time += 0.5f;
            }
            else
            {
                if (i % 2 == 0)
                {
                    value = -10;
                }
                else
                {
                    value = 10;
                }

                Keyframe key = new Keyframe(time, value);
                resetZCurve.AddKey(key);
                time += 0.75f;
            }

        }

        time = 0;

        //Xrot
        for (int i = 0; i < 3; i++)
        {
            
                if (i % 2 == 0)
                {
                    value = 5;
                }
                else
                {
                    value = -5;
                }

                Keyframe key = new Keyframe(time, value);
                resetYCurve.AddKey(key);
                time += 0.1f;
            
       
        }

        //Yrot
        for (int i = 0; i < 4; i++)
        {
            if (i < 2)
            {
                if (i % 2 == 0)
                {
                    value = -20;
                }
                else
                {
                    value = 20;
                }

                Keyframe key = new Keyframe(time, value);
                resetXCurve.AddKey(key);
                time += 0.5f;
            }
            else
            {
                if (i % 2 == 0)
                {
                    value = -10;
                }
                else
                {
                    value = 10;
                }

                Keyframe key = new Keyframe(time, value);
                resetXCurve.AddKey(key);
                time += 0.75f;
            }
        }
    }

    // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonUp(0))
            {
                StartCoroutine(SetActive());

            }

            
        }


        private void FixedUpdate()
        {
            if(Activate)     StartCoroutine(OneShot());
        
        }


        IEnumerator SetActive()
        {
            int timer = 0;
            while (timer < 4)
            {
                Activate = true;
                timer++;
                yield return null;
            }
            Activate = false;
        }


        IEnumerator OneShot()
        {
            float timerForAnim = 0;
            float timer = 0;
            float elapsedtime = 1f;
            while (timer < elapsedtime)
            {
                timer += Time.fixedDeltaTime;

                timerForAnim += Time.fixedDeltaTime;
                recamountYRot = resetYCurve.Evaluate(timerForAnim);
                recamountZRot = resetZCurve.Evaluate(timerForAnim);
                recamountXRot = resetXCurve.Evaluate(timerForAnim);
                Quaternion targetRotAfterShot =
                    Quaternion.Euler(new Vector3(recamountXRot, recamountYRot, recamountZRot));
                transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotAfterShot,
                    Time.deltaTime * 3);
                yield return null;
            }
            
            
        }
    
}
