using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class UIInputController : MonoBehaviour
{
    public static bool isActive;
    // Start is called before the first frame update
    private bool inputUnblocked;


    private Context context;

    [SerializeField] private GameObject inventoryHolder;
  [SerializeField]  private GameObject _dvc;
  [SerializeField] private GameObject restartMenu;


    public class Context
    {

        public enum  States
        {
            AllClosedState,
            InvOpenState,
            EscMenuOpenedState
        }

        private States state;

        public States State
        {
            get { return state; }

        }
        

        public Context()
        {
            state = States.AllClosedState;
        }

        public  void Request(States state)
        {
               this.state = state;
        }
    }

    void GetState()
    {
        
    }
    
   
    
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        inputUnblocked = true;
        context  = new Context();
        context.Request(Context.States.AllClosedState);

    }

   
    void Update()
    {
        GetState();
        if (inventoryHolder.activeSelf)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
     
       
        if (Input.GetKeyDown(KeyCode.Escape) && context.State != Context.States.AllClosedState)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Debug.Log("zex");
            restartMenu.SetActive(false);
            context.Request(Context.States.AllClosedState);
          GlobalFuncs.UnlockInput();
            _dvc.SetActive(false);
            inventoryHolder.SetActive(false);
         
            Cursor.visible = false;

        }


        if (Input.GetKeyDown(KeyCode.LeftAlt) && context.State == Context.States.AllClosedState)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            GlobalFuncs.LockInput();
            restartMenu.SetActive(true);
            context.Request(Context.States.EscMenuOpenedState);
            
        }
        
        
        if (Input.GetKeyDown(KeyCode.I) && context.State == Context.States.AllClosedState )
        {
         
           GlobalFuncs.LockInput();
           context.Request(Context.States.InvOpenState);
           inventoryHolder.SetActive(true);
     
        }

     
    
    }



   
}
