using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoSway : MonoBehaviour
{
    // Start is called before the first frame update


    [SerializeField] private Gundata gundata;
    [Header("Input")]
    private float inputX;
    private float inputY;
    
   
    [Header("Rotation")]
    private float posX;
    private float posY;


    private Quaternion initRot;
    private Vector3 initPos;

    private Vector3 targetPosition;
    void Start()
    {
        initPos = transform.localPosition;
    }

  void  OnEnable()
  {
        initPos = gundata.initAmmoHUDPos;
    }

    // Update is called once per frame
    void Update()
    {
       
        MyInput();

        posY += inputX;
        posX -= inputY;
       

        if (Mathf.Abs(inputY) == 0)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, initPos, Time.deltaTime * 2);
        }
        else
        {
            posY = Mathf.Clamp(posY,  initPos.y-0.005f,initPos.y+0.005f );
            targetPosition = new Vector3(gundata.initAmmoHUDPos.x, posY, initPos.z);
            transform.localPosition = Vector3.Lerp(transform.localPosition, targetPosition, Time.deltaTime * 10);
           
        }
    }
    
    void MyInput()
    {
        inputX = Input.GetAxis("Mouse X") * Time.deltaTime ;
        inputY = Input.GetAxis("Mouse Y") *  Time.deltaTime ;
    }
}
