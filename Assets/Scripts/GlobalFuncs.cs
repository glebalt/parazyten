using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalFuncs : MonoBehaviour
{
 
    // Start is called before the first frame update

 [SerializeField] private GameObject Player;
 private Rigidbody playerRb;
  [SerializeField] private GameObject cameraHolder;
  [SerializeField] private LootData nullObject;

  private static PlayerControllee  playerScript;
  private  static CamPivotRot camRotation;
  private static Gun gunRef;
  
  private PlayerControllee playerControlleeScriptReference;
  [Header("GlobalVars")] public static bool isPlayerSprinting;
  public static float playerXInput;
  public static float playerZInput;

  public static float playerCameraRotX;
  public static float playerCameraRotY;
  //Guns
  public static bool isPlayerShooting;
  public static bool isPlayerReloading;
  public static bool isPlayerRunning;
  //PlayerState
  public static bool isInInteractionMode;

  [SerializeField] private LayerMask Ground;

[Header("Audio")]
  [SerializeField] private AudioSource footStepsSource;

  [SerializeField] private AudioClip[] footstepsLabs;

  [SerializeField] private AudioClip[] footstepsGrass;

  [Header("Ambience")] [SerializeField] private AudioClip ambientClip;
     [SerializeField] private AudioSource ambienceSource;
 [SerializeField] private AudioSource ServersSource;
 

  private float footStepsTimer;
  private float timerDelay;

[Header("CamShake")]
  [SerializeField] private CameraShaker CameraShaker;
  private float timerForCamShake;
  float timerToEx = 0;
  float timerToSptrint = 0;
  
  
  
  //Hands


 [SerializeField] private List<GameObject> weaponHolders_List = new List<GameObject>();
    void Start()
    {
  
        
        
        ambienceSource.clip = ambientClip;
        InvokeRepeating("PlayAmbient",0,300f);


        timerForCamShake = 0;
        
        playerRb = Player.GetComponent<Rigidbody>();
       
        playerScript = Player.GetComponent<PlayerControllee>();
        camRotation = cameraHolder.GetComponent<CamPivotRot>();
        
        playerControlleeScriptReference = Player.GetComponent<PlayerControllee>();
    }

    // Update is called once per frame
    void Update()
    {
        
       
     
        if (playerRb.velocity.magnitude > 0.2 && isPlayerRunning == false && !isPlayerShooting)
        {
            timerToEx += Time.deltaTime;
            if (timerToEx > 0.8)
            {
               timerToEx = 0;
                CameraShaker.CallCameraShakeWalking(8);
            }
            
        }

        if (Input.GetKey(KeyCode.LeftShift) && playerRb.velocity.magnitude > 7 && !isPlayerShooting)
        {
            timerToSptrint += Time.deltaTime;
            isPlayerRunning = true;
            CameraShaker.CamRun(timerToSptrint);

            if (timerToSptrint > 5)
            {
                timerToSptrint = 0;
            }
        }
        else
        {
            isPlayerRunning = false;
        }
        
        PlayAudio();
        
    gunRef = FindActiveGundata();

    isInInteractionMode = InteractionScript.inInteractMode;
    isPlayerSprinting = playerControlleeScriptReference.IsSprinting();
    playerXInput = playerControlleeScriptReference.XInp();
    playerZInput = playerControlleeScriptReference.ZInp();

    playerCameraRotX = camRotation.CameraRotX();
    playerCameraRotY = camRotation.CameraRotY();

    }


    public static void LockInput()
    {
        gunRef.enabled = false;
        playerScript.enabled = false;
        camRotation.enabled = false;
    }
    
    public static void UnlockInput()
    {
        gunRef.enabled = true;
        playerScript.enabled = true;
        camRotation.enabled = true;
    }
    
    Gun FindActiveGundata()
    {
        var objects = FindObjectsOfType<Gun>();
        foreach(var obj in objects)
        {
            if (obj.gameObject.activeInHierarchy)
            {
                return obj;
            }
        }

        return null;
    }

    private void PlayAudio()
    {
     
        Vector3 worldVelocity = playerRb.velocity;
        Vector3 localVelocity = transform.InverseTransformDirection(worldVelocity);
        
       footStepsTimer += Time.deltaTime;
        RaycastHit hit;
 
        if (Physics.Raycast(Player.transform.position, Vector3.down, out hit, 3f, Ground) && Mathf.Abs(playerXInput) > 0 ||Mathf.Abs(playerZInput) > 0 )
        {
            string groundType = hit.collider.gameObject.tag;
            if (Mathf.Abs(localVelocity.magnitude) > 6.5)
            {
                timerDelay = 0.35f;
            }
            else
            {
                timerDelay = 0.8f;
            }
     
            if ( footStepsTimer > timerDelay)
            {
                footStepsTimer = 0;
                PickFootStepClip(groundType);
                footStepsSource.Play();
            }
          
        }
    }



    void PickFootStepClip(string groundType)
    {
        if (groundType == "LabsFloorType")
        {
            footStepsSource.clip = footstepsLabs[Random.Range(0, 6)];
        }

        if (groundType == "GrassType")
        {
            footStepsSource.clip = footstepsGrass[Random.Range(0, 6)];
        }
    }


    void PlayAmbient()
    {
       ambienceSource.Play();
       ServersSource.Play();
    }



    public void DisableWeaponHolders()
    {
        foreach (var wepHolder in weaponHolders_List)
        {
           wepHolder.SetActive(false);
        }
    }
    
    public void EnableWeaponHolders()
    {
       weaponHolders_List[0].SetActive(true);
    }
    
}
