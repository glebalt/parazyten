using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamPivotRot : MonoBehaviour
{
    // Start is called before the first frame update
    [Header("Input")]
    private float inputX;
    private float inputY;
    
   
    [Header("Rotation")]
    private float rotationX;
    private float rotY;

    public float CameraRotX()
    {
        return rotationX;
    }
    
    public float CameraRotY()
    {
        return rotY;
    }
    [SerializeField] private float sens = 50;
    public GameObject weaponHolder;
    private void Start()
    {
   //   Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        
        if(GlobalFuncs.isInInteractionMode == false)
        {
            MyInput();
            
            rotY += inputX * sens;
            rotationX -= inputY * sens;
       
            rotationX = Mathf.Clamp(rotationX, -90f, 90f);
       
       
            transform.rotation = Quaternion.Euler(rotationX,rotY,0);
        }
        
  

        

    }

    void MyInput()
    {
        inputX = Input.GetAxis("Mouse X") * sens * Time.deltaTime;
        inputY = Input.GetAxis("Mouse Y") * sens * Time.deltaTime;
    }
}
