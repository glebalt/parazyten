using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

public class WeaponHolderDelay : MonoBehaviour
{
    [SerializeField] private GameObject camholder;
    // Start is called before the first frame update
    
  
    
    private Vector3 targetPos;
    private Quaternion targetRot;
    
  
    float timerForEval;
    private float recamountZPos;
    private float recamountXRot;
    private float recamountZRot;

    [Header("Anim curves")] public AnimationCurve zTransformCurve;
    public AnimationCurve XRotCurve;
    public AnimationCurve ZRotCurve;
    public AnimationCurve xTransformCurve;
    
    private Vector3 initPos;
    private Quaternion initRot;

    private float timer;
    [SerializeField] private GameObject gunBackLink;
    
    //mosueInp
    private float mouseX;
    private float mouseY;

    private float xRot;
    private float yRot;

    
    void Start()
    {
     
        initPos = transform.localPosition;
        initRot = transform.localRotation;
        float time = 0;
        float value;
        
        //rotZ
        for (int i = 0; i < 41; i++)
        {
            if (i % 2 == 0)
            {
                value = -359;
            }
            else
            {
                value = -350;
            }

            Keyframe key = new Keyframe(time, value);
            ZRotCurve.AddKey(key);
            time += 0.25f;
         
        }

        time = 0;
        //xRot Curve
        for (int i = 0; i < 41; i++)
        {
            if (i % 2 == 0)
            {
                value = -6;
            }
            else
            {
                value = 3;
            }

            Keyframe key = new Keyframe(time, value);
            XRotCurve.AddKey(key);
            time += 0.05f;
         
        }

        time = 0;
        //ZTransformCurve
        for (int i = 0; i < 41; i++)
        {
            if (i % 2 == 0)
            {
                value = 0.50f;
            }
            else
            {
                value = 0.45f;
            }

            Keyframe key = new Keyframe(time, value);
          zTransformCurve.AddKey(key);
            time += 0.05f;
         
        }
        
        time = 0;
        //XTransformCurve
        for (int i = 0; i < 41; i++)
        {
            if (i % 2 == 0)
            {
                value = 0.39f;
            }
            else
            {
                value = 0.43f;
            }

            Keyframe key = new Keyframe(time, value);
            xTransformCurve.AddKey(key);
            time += 0.05f;
         
        }
        
    }


    // Update is called once per frame
    void FixedUpdate()
    {
     //   transform.localRotation = Quaternion.Slerp(transform.localRotation,camholder.transform.rotation, Time.deltaTime * 3);
    
       if (Input.GetKey(KeyCode.Mouse0) && GlobalFuncs.isPlayerShooting)
       {
         timer += Time.fixedDeltaTime;
         
         if (timer > 1) timer = 0;
         
         recamountZRot = ZRotCurve.Evaluate(timer);
         recamountZPos = zTransformCurve.Evaluate(timer);
         recamountXRot = XRotCurve.Evaluate(timer);
        
                GetPos();
                GetRot();
             //  transform.localPosition = Vector3.Lerp(transform.localPosition,targetPos,Time.deltaTime * 20);
               transform.localRotation =  Quaternion.Slerp(transform.localRotation, targetRot,
             Time.deltaTime  * 20 );
       }
      

      
       
       
       if (mouseY == 0 && mouseX == 0 )
       {
           transform.localRotation = Quaternion.Slerp(transform.localRotation,initRot, Time.deltaTime * 10);
           transform.localPosition = Vector3.Lerp(transform.localPosition, initPos, Time.deltaTime * 10);
       }
     
     
    }



    Quaternion GetMouseRot()
    {
        mouseX = Input.GetAxis("Mouse X") * 10000 * Time.deltaTime;
       mouseY = Input.GetAxis("Mouse Y") * 10000 * Time.deltaTime;

       xRot -= mouseY;
       yRot += mouseX;

       xRot = Mathf.Clamp(xRot, 0.5f, 2f);
       yRot = Mathf.Clamp(yRot, 9, 16);
       return Quaternion.Euler(-xRot,yRot,0);
    }
    
  void GetRot()
    {
      targetRot =  Quaternion.Euler(new Vector3(initRot.x,12,recamountZRot));
    }

  void GetPos()
    {
        targetPos  = new Vector3(initPos.x,initPos.y,recamountZPos  );
    }
}
