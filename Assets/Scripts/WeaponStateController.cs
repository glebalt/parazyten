using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;


public class WeaponStateController : MonoBehaviour
{
    public GameObject slot1;
    public GameObject slot2;
    public GameObject slot3;
    private int activeSlot;
   
    private void Start()
    {
        slot1.SetActive(true);
        slot2.SetActive(false);
        slot3.SetActive(false);
        activeSlot = 1;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) && activeSlot != 1)
        {
            activeSlot = 1;
            ClearAll();
            slot1.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) && activeSlot != 2)
        {
            activeSlot = 2;
            ClearAll();
            slot2.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3) && activeSlot != 3)
        {
            activeSlot = 3;
            ClearAll();
            slot3.SetActive(true);
        }
        
        
    }

    void ClearAll()
    {
        slot1.SetActive(false);
        slot2.SetActive(false);
        slot3.SetActive(false);
    }
   
}
