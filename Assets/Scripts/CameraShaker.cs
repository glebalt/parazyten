using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CameraShaker : MonoBehaviour
{
    //curves
 
   
  public AnimationCurve curve;

  public AnimationCurve xRotRecoilRSH12;
  public AnimationCurve zRotRecoilRSH12;

  public AnimationCurve CameraShakeWalkingZ;
  public AnimationCurve CameraShakeWalkingX;


  public AnimationCurve CameraShakeInventorySwitchRight;
  public AnimationCurve CameraShakeInventorySwitchLeft;
  public AnimationCurve CameraShakeInventoryDrop;

    private Vector3 targetPos;
    private Quaternion targetRot;

    private Quaternion targetRotRSH12;



    
    private float recoilShakeRSH12X;
    private float recoilShakeRSH12Z;
    
    //CameraShakeWalking

    private float cameraShakeWalkingRotZ;
    private float cameraShakeWalkingRotX;
    
    //CameraInventoryShake
    private float camInvShakeSwitchRightX;
    private float camInvShakeSwitchRightZ;
    
    private float camInvShakeSwitchLeftX;
    private float camInvShakeSwitchLeftZ;
    
    private float camMediumShakeX;


    // Start is called before the first frame update
    void Start()
    {
     
        curve.ClearKeys();
        float time = 0;
        float value = 0;
        for (int i = 0; i < 41; i++)
        {
            if (i % 2 == 0)
            {
                value = -0.01f;
            }
            else
            {
                value = 0.01f;
            }

            Keyframe key = new Keyframe(time, value);
            curve.AddKey(key);
            time += 0.25f;
         
        }
        
        
        //ReloadCurves
        //Xrot
        
     
      
       

       //XrotShake

     
        
        //RSH and XM Recoil
        time = 0;
        value = 0;
        
        for (int i = 0; i < 50; i++)
        {
           
            
                value -=3f;
           
        
            Keyframe key = new Keyframe(time, value);
            xRotRecoilRSH12.AddKey(key);
            time += 0.12f;
         
        }
        
        time = 0;
        value = 0;
        
        for (int i = 0; i < 5; i++)
        {
            if (i % 2 == 0)
            {
                value = -3f;
            }
            else
            {
                value = 3f;
            }

            Keyframe key = new Keyframe(time, value);
            zRotRecoilRSH12.AddKey(key);
            time += 0.25f;
         
        }
        
        //CameraShakeWalkingMotion
        
        time = 0;
        value = 0;
        
        for (int i = 0; i < 100; i++)
        {
            if (i % 2 == 0)
            {
                value = -2f;
            }
            else
            {
                value = 2f;
            }

            Keyframe key = new Keyframe(time, value);
            CameraShakeWalkingZ.AddKey(key);
            time += 0.8f;
         
        }
        
        for (int i = 0; i < 100; i++)
        {
            if (i % 2 == 0)
            {
                value = -2f;
            }
            else
            {
                value = 2f;
            }

            Keyframe key = new Keyframe(time, value);
            CameraShakeWalkingX.AddKey(key);
            time += 0.8f;
         
        }
        
        
        time = 0;
        value = 0;
        
    
        
        time = 0;
        value = 0;
        
        //camInventory
        for (int i = 0; i < 1000; i++)
        {
            if (i % 2 == 0)
            {
                value = -1f;
            }
            else
            {
                value = 1f;
            }

            Keyframe key = new Keyframe(time, value);
            CameraShakeInventoryDrop.AddKey(key);
            time += 0.15f;
         
        }
        
        
        
        time = 0;
        value = 0;

        for (int i = 0; i < 1000; i++)
        {
           
                value -= -0.3f;
                

            Keyframe key = new Keyframe(time, value);
            CameraShakeInventorySwitchRight.AddKey(key);
            time += 0.15f;
         
        }
        
        
        time = 0;
        value = 0;

        for (int i = 0; i < 1000; i++)
        {
            
           
            value += 0.3f;
                

            Keyframe key = new Keyframe(time, value);
            CameraShakeInventorySwitchLeft.AddKey(key);
            time += 0.15f;
         
        }
        
    }

    private void OnEnable()
    {
       
    }


    // Update is called once per frame
    void Update()
    {

   
        if (!InteractionScript.inInteractMode && Input.GetKey(KeyCode.Mouse1))
        {
            transform.gameObject.GetComponent<Camera>().fieldOfView =
                Mathf.Lerp(transform.gameObject.GetComponent<Camera>().fieldOfView, 30, Time.deltaTime * 8);
        }
        else if (!InteractionScript.inInteractMode)
        {
            transform.gameObject.GetComponent<Camera>().fieldOfView =
                Mathf.Lerp(transform.gameObject.GetComponent<Camera>().fieldOfView, 60, Time.deltaTime * 8);
        }
   
     
        

    }

  public  void CallCamShakeMedium()
    {
        StartCoroutine(CameraShakeMedium());
    }

    IEnumerator CameraShakeMedium()
    {
        float timer = 0;

        while (timer < .5f)
        {
           camMediumShakeX = CameraShakeInventoryDrop.Evaluate(timer);
            timer += Time.deltaTime * 2;
           Quaternion camInvTargetRot = Quaternion.Euler(camMediumShakeX,0,0);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, camInvTargetRot, Time.deltaTime * 20 );
            yield return null;
        }

        timer = 0;
        while (timer < .3f)
        {
            timer += Time.fixedDeltaTime;
            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0,0,0), Time.deltaTime * 20 );
            yield return null;
        }
    }



    public void CallCamInvRightSwitch()
    {
        StartCoroutine(CameraShakeInvRight());
    }
    IEnumerator CameraShakeInvRight()
    {
        float timer = 0;

        while (timer < 0.15f)
        {
            timer += Time.deltaTime;
            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0,0,-6), Time.deltaTime * 10 );
            yield return null;
        }
        
        timer = 0;
        while (timer < .25f)
        {
            timer += Time.fixedDeltaTime;
            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0,0,0), Time.deltaTime * 20 );
            yield return null;
        }
        
        
    }

    public void CallCamInvLeftSwitch()
    {
        StartCoroutine(CameraShakeInvLeft());
    }
    
    
    IEnumerator CameraShakeInvLeft()
    {
        float timer = 0;

        while (timer < 0.15f)
        {
            timer += Time.deltaTime;
            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0,0,6), Time.deltaTime * 10 );
            yield return null;
        }
        
        timer = 0;
        while (timer < .25f)
        {
            timer += Time.fixedDeltaTime;
            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0,0,0), Time.deltaTime * 20 );
            yield return null;
        }
        
        
    }

   

    void GetRecoilRSH12()
    {
        targetRotRSH12 = Quaternion.Euler(recoilShakeRSH12X,0,recoilShakeRSH12Z);
    }
    



    IEnumerator RecoilRSH12()
    {
        float specialTimer = 0;

        while (specialTimer < .5f)
        {
           recoilShakeRSH12X = xRotRecoilRSH12.Evaluate(specialTimer);
           recoilShakeRSH12Z = zRotRecoilRSH12.Evaluate(specialTimer);
;            specialTimer += Time.fixedDeltaTime;
            GetRecoilRSH12();
            transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotRSH12, Time.fixedDeltaTime * 20 );
            yield return null;
        }

        specialTimer = 0;
  
        while (specialTimer < 1f)
        {
            specialTimer += Time.fixedDeltaTime;
            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0,0,0), Time.fixedDeltaTime * 3 );
            yield return null;
        }
        
    }

    private IEnumerator ResetCam()
    {
        float specialTimer = 0;
        while (specialTimer < 1f)
        {
            specialTimer += Time.fixedDeltaTime;
            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0,0,0), Time.fixedDeltaTime * 3 );
            yield return null;
        }
    }

    public void CallCameraShakeWalking(float speed)
    {
        StartCoroutine(CameraShakeWalk(speed));
    }
    


    IEnumerator CameraShakeWalk( float speed)
    {
        float timer = 0;
        while (timer < 0.5f)
        {
            timer += Time.deltaTime;
            cameraShakeWalkingRotZ = CameraShakeWalkingZ.Evaluate(timer);
            cameraShakeWalkingRotX = CameraShakeWalkingX.Evaluate(timer);
            Quaternion targetRotCamera = Quaternion.Euler(cameraShakeWalkingRotX,0,cameraShakeWalkingRotZ);
            transform.localRotation = Quaternion.Slerp(transform.localRotation,targetRotCamera,Time.deltaTime * speed);
            yield return null;
        }

        timer = 0;
        while (timer < 0.5f)
        {
            timer += Time.deltaTime;
            transform.localRotation = Quaternion.Slerp(transform.localRotation,Quaternion.Euler(0,0,0), Time.deltaTime * speed);
            yield return null;
        }
    }




  public  void CamRun(float timer)
    {
        timer += Time.deltaTime;
        cameraShakeWalkingRotZ = CameraShakeWalkingZ.Evaluate(timer);
        cameraShakeWalkingRotX = CameraShakeWalkingX.Evaluate(timer);
        Quaternion targetRotCamera = Quaternion.Euler(cameraShakeWalkingRotX,0,cameraShakeWalkingRotZ);
        transform.localRotation = Quaternion.Slerp(transform.localRotation,targetRotCamera,Time.deltaTime * 20);
   
    }

    public void CallRecoilRSH12()
    {
        StartCoroutine(RecoilRSH12());
    }

    public void CallResetCam()
    {
        StartCoroutine(ResetCam());
    }

    public void CallZoomCam()
    {
        StartCoroutine(ZoomCam());
    }

    public void CallUnzoomCam()
    {
        StartCoroutine(UnzoomCam());
    }


    IEnumerator ZoomCam()
    {
     float timer = 0;
     while (timer < 0.5)
     {
         timer += Time.deltaTime;
         transform.gameObject.GetComponent<Camera>().fieldOfView =
             Mathf.Lerp(transform.gameObject.GetComponent<Camera>().fieldOfView, 30, Time.deltaTime * 8);
         yield return null;
     }
    }
    
    IEnumerator UnzoomCam()
    {
        float timer = 0;
        while (timer < 0.5)
        {
            timer += Time.deltaTime;
            transform.gameObject.GetComponent<Camera>().fieldOfView =
                Mathf.Lerp(transform.gameObject.GetComponent<Camera>().fieldOfView, 60, Time.deltaTime * 8);
            yield return null;
        }
    }
    
}
