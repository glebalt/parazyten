using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllee : MonoBehaviour
{
    // Start is called before the first frame update
    
    [Header("Movement")]
    private Vector3 moveDirection;
    private float xInp;
    private float zInp;
   
    public Rigidbody rb;
    private CapsuleCollider playerCol;
    private bool isGrounded;
   public LayerMask ground;
   private bool isSprinting;
   private bool isCrouching;

   
  
   [SerializeField] private GameObject camHolder;
   [SerializeField] private Camera playerCamera;
   
   
   //speed
   public float sprintSpeed;
   public float walkSpeed;

  public float staminaAmount;
   private float timerForSprint;
   private float timerForSprintRegen;


   public bool IsSprinting()
   {
       return isSprinting;
   }

   public float XInp()
   {
       return xInp;
   }
   
   public float ZInp()
   {
       return zInp; 
   }

   public bool IsCrouching()
   {
       return isCrouching;
   }
    [Header("Camera Pivot")] public Transform camPivot;
    void Start()
    {
      
        playerCol = GetComponent<CapsuleCollider>();
        staminaAmount = 25;
        Physics.gravity = new Vector3(0, -20, 0);
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        sprintSpeed = 55;
        walkSpeed = 25;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            StartCoroutine(RegenerateStamina());
        }
            
            
        MyInput();
        if (Input.GetKey(KeyCode.LeftShift) && CanSrint())
        {
           
            timerForSprint += Time.deltaTime;
            if (timerForSprint > 0.05f)
            {
                timerForSprint = 0;
                staminaAmount -= 0.5f;
                staminaAmount = Mathf.Clamp(staminaAmount, 0, 25);
            }
              
        }
    

 
      
        Crouch();
        isSprinting = Input.GetKey(KeyCode.LeftShift);
        isGrounded =  Physics.Raycast(transform.position, Vector3.down, 3f,ground);
        Jump();
      
        CheckForWall();
      
      
        camPivot.position = transform.position;

    }

    private void FixedUpdate()
    {
      
        if (GlobalFuncs.isInInteractionMode == false)
        {

            if (Input.GetKey(KeyCode.LeftShift) && CanSrint())
            {
                PlayerSprint();
            }
            else
            {
                MovePlayer();
            }
        }

    }


    void Crouch()
    {
        if (Input.GetKey(KeyCode.LeftControl))
        {
            isCrouching = true;
            walkSpeed = 5;
            playerCol.height = 0.5f;
            Vector3 target = new Vector3(playerCamera.transform.localPosition.x,-0.25f,
                playerCamera.transform.localPosition.z);
            playerCamera.transform.localPosition =
                Vector3.Lerp(playerCamera.transform.localPosition, target, Time.deltaTime * 5);
        }
        else
        {
            isCrouching = false;
            playerCol.height = 1;
            playerCamera.transform.localPosition =  Vector3.Lerp(playerCamera.transform.localPosition, new Vector3(0,0,0),
                Time.deltaTime * 5);
            walkSpeed = 25;
        }
    }

 

    
    
    IEnumerator RegenerateStamina()
    {
        yield return new WaitForSeconds(1f);
        float timer = 0;
        while (timer < 1f)
        {
            timer += Time.deltaTime;
            yield return new WaitForSeconds(0.05f);
            staminaAmount += 1;
            staminaAmount = Mathf.Clamp(staminaAmount, 0, 25);
            
            if (Input.GetKey(KeyCode.LeftShift) && CanSrint())
            {
                yield break;
            }

            yield return null;
        }
    }


 

    bool CanSrint() => staminaAmount >= 1;

    private void MyInput()
    {
        xInp = Input.GetAxisRaw("Vertical");
        zInp = Input.GetAxisRaw("Horizontal");

    }

    void MovePlayer()
    {
        moveDirection = camPivot.forward * xInp + camPivot.right * zInp;
        moveDirection.y = 0;
      
          rb.AddForce(moveDirection.normalized * walkSpeed,ForceMode.Force);
          
       
       
    }

    void Jump()
    {
        
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
          
            rb.AddForce(Vector3.up * 15,ForceMode.Impulse);
        }

        
    }

    void PlayerSprint()
    {
        
            moveDirection = camPivot.forward * xInp + camPivot.right * zInp;
            moveDirection.y = 0;
            rb.AddForce(moveDirection.normalized * sprintSpeed,ForceMode.Force);
            isSprinting = true;
        

        
    }


    void CheckForWall()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, moveDirection, out hit, 2f))
        {
          if (hit.transform.gameObject.CompareTag("LevelObstacle"))
          {
              sprintSpeed = 0;
              walkSpeed = 0;
                Debug.Log("Colliding");
          }

        }
        else if (isCrouching)
        {
            sprintSpeed = 15;
            walkSpeed = 15;
        }
        else
        {
            sprintSpeed = 55;
            walkSpeed = 25;
        }
       
    }


 
    
}
