using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponHolderMP9Pivot : MonoBehaviour
{
    //mosueInp
    private float mouseX;
    private float mouseY;

    private float xRot;
    private float yRot;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.localRotation = Quaternion.Slerp(transform.localRotation,GetMouseRot(),Time.deltaTime * 3);
    }
    
    Quaternion GetMouseRot()
    {
        mouseX = Input.GetAxis("Mouse X") * 20 * Time.deltaTime;
        mouseY = Input.GetAxis("Mouse Y") * 20 * Time.deltaTime;

        xRot -= mouseY;
        yRot += mouseX;

        xRot = Mathf.Clamp(xRot, 0.5f, 2f);
        yRot = Mathf.Clamp(yRot, 5, 10);
        return Quaternion.Euler(0,0,0);
    }
}
