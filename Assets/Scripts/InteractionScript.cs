using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionScript : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private GameObject cam;
    [SerializeField] private GameObject camHolder;


 [SerializeField]   private LayerMask loot;
 [SerializeField] private LayerMask Interactable;
 [SerializeField] private GlobalFuncs globalController;


public static bool inInteractMode;
private Vector3 currentObjInitPos;
private GameObject currentSelected;
private Vector3 lastPlayerPos;


public static Button_Radio currentButton;

[Header("Render positions")]





//Inventory
    [SerializeField] private InventoryHandler inventoryHandler;
    
    //States
    public enum CurrentPlayerInteractionState
    {
        InInteractionMode,
        InInventory,
        Idle
    }

    public static CurrentPlayerInteractionState currentInteractionState;

    
    //Cam
    [SerializeField] private CameraShaker cameraShaker;

    void Start()
    {
        inInteractMode = false;
        GameObject gmb = new GameObject();

    }

    // Update is called once per frame
    void Update()
    {
       
        TakeLoot();
        InteractMode();
        ExitIntMod();
       
    }

    void TakeLoot()
    {   
      bool hitted =  Physics.Raycast(transform.position, cam.transform.forward, out RaycastHit hit, 10f,loot);

      if (hitted && Input.GetKeyDown(KeyCode.E))
      {
        

       Loot lootItem = hit.transform.gameObject.GetComponent<Loot>();
       GameObject lootItemTransform = hit.collider.gameObject;


       switch (   inventoryHandler.AddItem(lootItem,lootItemTransform))
       {
           case InventoryHandler.InventoryState.SameItemAddedSuccess:
               Destroy(lootItemTransform);
               break;
           case InventoryHandler.InventoryState.NewSlotCreatedSuccess:
       
               lootItemTransform.GetComponent<Rigidbody>().isKinematic = true;
               break;
           case InventoryHandler.InventoryState.NotEnoughSpace:
               break;
       }
    
      
      
      }
    }


    void InteractMode()
    {   
   ;
        bool hitted =  Physics.Raycast(transform.position, cam.transform.forward, out RaycastHit hit, 2f,Interactable);

        if (hitted && Input.GetKeyDown(KeyCode.E))
        {
            cameraShaker.CallZoomCam();
            globalController.DisableWeaponHolders();
            currentSelected = hit.transform.gameObject;
            StartCoroutine(AttachCam());
            inInteractMode = true;
            Cursor.lockState = CursorLockMode.None;
           lastPlayerPos = transform.position;
         

           Transform currentObj = hit.transform.Find("ButtonStart_DoorUnlocker");

           currentButton  = currentObj.gameObject.GetComponent<Button_Radio>();
           
          
    
      


        }
    }

    private void ExitIntMod()
    {
        if (inInteractMode && Input.GetKeyDown(KeyCode.Escape))
        {
            cameraShaker.CallUnzoomCam();
            globalController.EnableWeaponHolders();
            inInteractMode = false;
            Cursor.lockState = CursorLockMode.Locked;
            GlobalFuncs.UnlockInput();
        
        }
    }

    bool CanInteract() => !inInteractMode;

    bool CanOpenInv()
    {
        return Convert.ToBoolean(0);
    }

    IEnumerator AttachCam()
    {
        float timer = 0;
        while (timer < .5f)
        {
            timer += Time.deltaTime;
            
            transform.position = Vector3.Lerp(transform.position,
                new Vector3(currentSelected.transform.position.x - 2, lastPlayerPos.y,
                    currentSelected.transform.position.z), Time.deltaTime * 5);
            
           
            Vector3 destination = currentSelected.transform.position - camHolder.transform.position;
            camHolder.transform.rotation = Quaternion.Euler(camHolder.transform.rotation.x,
                Vector3.Angle(camHolder.transform.position,destination),camHolder.transform.rotation.z);
            camHolder.transform.rotation = Quaternion.Euler(5.5f, 89, 0);
            yield return null;
        }
      
        
    }



 
}
