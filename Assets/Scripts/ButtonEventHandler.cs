using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonEventHandler : MonoBehaviour,IBeginDragHandler,IEndDragHandler,IPointerEnterHandler,IDragHandler
{

    private RectTransform pos;

    [SerializeField] private Camera playerCam;
    [Header("Null object referecnce")]
    [SerializeField] private LootData nullObject;

    [Header("loot in current cell")] public LootData loot;

   [SerializeField] private int buttonSlotNumber;
    [SerializeField] private Vector2 init;
    // Start is called before the first frame update
    void Awake()
    {
       // InventoryReference.AddComponent<Inventory>();
        pos = GetComponent<RectTransform>();

      
    }

    // Update is called once per frame
    void Update()
    {
      
    
    }

    public void OnPointerEnter(PointerEventData eventData)
    {

        Debug.Log(pos.anchoredPosition);
    }



 public void OnBeginDrag(PointerEventData eventData)
 {
     //getInfo
     
     Debug.Log("drag begin");
    
 }

 public void OnEndDrag(PointerEventData eventData)
 {
     Debug.Log("drag ended-");
   
     if (OutOfBounds() )
     {
         //spawnInstance
         GameObject gmb = Instantiate(loot.prefab, new Vector3(playerCam.transform.transform.position.x,5,playerCam.transform.position.z - 2), Quaternion.Euler(0, 0, 0));
         gmb.layer = 7;
         pos.anchoredPosition = init;
   
           Gun gun = FindActiveGundata();
          
     }
 }

 public void OnDrag(PointerEventData eventData)
 {
   
     {
         pos.anchoredPosition += eventData.delta;
     }
    
 }


bool OutOfBounds()
{

     if (pos.anchoredPosition.x > 1300 )
     {

         return true;
         
     }
     else
     {
         return false;
       
     }
     
}

Gun FindActiveGundata()
{
    var objects = FindObjectsOfType<Gun>();
    foreach(var obj in objects)
    {
        if (obj.gameObject.activeInHierarchy)
        {
            return obj;
        }
    }

    return null;
}
 
}
