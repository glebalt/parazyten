using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponHolderShotgun : MonoBehaviour
{
    private float recamountZPos;
    private float recamountXRot;

    private float timer;
    
    
    [Header("Anim curves")] public AnimationCurve zTransformCurve;
    public AnimationCurve XRotCurve;

    private Vector3 initPos;

    private Quaternion initRot;

    private Vector3 targetPos;

    private Quaternion targetRot;


    private float timerUpdate;
    // Start is called before the first frame update
    void Start()
    {
        timer = 0;
        initPos = transform.localPosition;
        initRot = transform.localRotation;
        float time = 0;
        float value =-0.2f;
        
        //TransformZ
        for (int i = 0; i < 3; i++)
        {
            
            Keyframe key = new Keyframe(time, value);
            zTransformCurve.AddKey(key);
            time += 0.15f;
            value -= 0.2f;

        }

        value = 0;
        for (int i = 3; i < 6; i++)
        {
            
            Keyframe key = new Keyframe(time, value);
            zTransformCurve.AddKey(key);
            time += 0.15f;
            value += 0.2f;

        }
        

        time = 0;
        value = 0;
        //xrot
        for (int i = 0; i < 3; i++)
        {
           
                Keyframe key = new Keyframe(time, value);
                XRotCurve.AddKey(key);
                time += 0.5f;
                value -= 14f;
            
        }

        value = 0;
        for (int i = 3; i < 6; i++)
        {
           
            Keyframe key = new Keyframe(time, value);
            XRotCurve.AddKey(key);
            time += 0.5f;
            value += 14f;
            
        }
        
    }

    // Update is called once per frame
 
    
    void Update()
    {

        timerUpdate += Time.fixedDeltaTime;
        
        if (Input.GetKeyDown(KeyCode.Mouse0) && timerUpdate > 2)
        {
            timerUpdate = 0;
            timer = 0;
            StartCoroutine(Shoot());
        }
      

        
    }

    
    IEnumerator Shoot()
    {
        recamountZPos = 0;
        while (timer < 2f)
        {
            Debug.Log(timer);
            timer += Time.fixedDeltaTime;
            recamountZPos = zTransformCurve.Evaluate(timer);
            recamountXRot = XRotCurve.Evaluate(timer);
        
            GetPos();
            GetRot();
          
            transform.localPosition = Vector3.Lerp(transform.localPosition,targetPos,Time.fixedDeltaTime * 5);
            transform.localRotation =  Quaternion.Slerp(transform.localRotation, targetRot,
                Time.fixedDeltaTime  * 5 );
            yield return null;
        }

        timer = 0;
        while (timer < 0.6f)
        {
            timer += Time.fixedDeltaTime;
            transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, Time.fixedDeltaTime * 5);
            transform.localRotation = Quaternion.Lerp(transform.localRotation,initRot,Time.fixedDeltaTime * 5);
            yield return null;
        }
     
       

      
     
    }
    
    void GetRot()
    {
        targetRot =  Quaternion.Euler(new Vector3(recamountXRot,0,0));
    }

    void GetPos()
    {
        targetPos  = new Vector3(transform.localPosition.x,-0.28f,recamountZPos  );
    }
}
