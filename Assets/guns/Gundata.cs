using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "Gun" , menuName = "Weapons")]
public class Gundata : ScriptableObject
{
 [Header("Main")]
  public int damage;
  public int currentAmmo;
  public int currentAmmoInMag;
  public float timeBetweenShot;
  public int magazineCapacity;
  public int ammoId;
  public int ammoRelaodAmount;
  [Header("Audio")]
 public  AudioClip shotClip;
 public  AudioClip reloadClip;
 public  AudioClip emptyClip;
 public AudioClip equipClip;
 [Header("Bools")]
  public bool isShooting;
  public bool isSemi;
  public bool isRelaoding;
 [Header("HUD")] public Vector3 initAmmoHUDPos;

 [Header("VFX")] public GameObject[] VFX;
}
