using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunState1 : GunStateMachine
{
 private GameObject firstSlot;
 private Animator anim;
  
  
  public GunState1(GameObject firstSlot, Animator anim){
    this.firstSlot = firstSlot;
    this.anim = anim;
  }


    
  
  protected override void Execute()
  {
    firstSlot.SetActive(true);
    
  }

  protected override void Hide()
  {
   anim.Rebind();
    firstSlot.SetActive(false);
  }
}
