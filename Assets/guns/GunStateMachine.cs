using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunStateMachine
{
  protected GunStateMachine currentState;

  private GameObject slot_1;
  private GameObject slot_2;
    private GunStateMachine firstSlot;
    private GunStateMachine secondSlot;

    protected bool stateChanged;
    private List<GunStateMachine> states;
    public GunStateMachine()
    {
        
    }

    public GunStateMachine(GameObject slot_1, GameObject secSlot, Animator anim1, Animator anim2)
    {
        firstSlot = new GunState1(slot_1,anim1);
        secondSlot = new GunState2(secSlot,anim2);
      
        currentState = secondSlot;
    }
    protected virtual void Execute()
    {
      
        Debug.Log("FAIL");
    }

    
   public void GetState()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) && currentState != firstSlot)
        {
            stateChanged = true;
            if (stateChanged)
            {
                currentState.Hide();
            }
            currentState = firstSlot;
            return;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2) && currentState != secondSlot)
        {
            stateChanged = true;
            if (stateChanged)
            {
                currentState.Hide();
            }
            currentState = secondSlot;
            return;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            
        }

        stateChanged = false;
    }


    protected virtual void Hide()
    {
        
    }

    public void Play()
    {
      
        currentState.Execute();
    }
}
