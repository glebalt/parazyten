using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunState2 : GunStateMachine
{
    private GameObject secSlot;
    private Animator anim;
  
  
    public GunState2(GameObject sec,Animator anim){
        this.secSlot= sec;
        this.anim = anim;
    }


    
  
    protected override void Execute()
    {
        secSlot.SetActive(true);
    }

    protected override void Hide()
    {
        anim.Rebind();
        secSlot.SetActive(false);
    }
}
