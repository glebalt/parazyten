
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;


public class Gun : MonoBehaviour
{
    // Start is called before the first frame update
    [Header("Gundata")]
    [SerializeField] private AudioSource audioSource;
   [SerializeField] private Gundata gundata;

 
   [SerializeField] private Camera playerCam;
   [SerializeField] private GameObject gun;
   [SerializeField] private WeaponHolderDelay controller;
   
   [Header("Refernces")] [SerializeField] private GameObject player;
   private GameObject firePoint;
   [SerializeField]  private GameObject crosshair;
   public LayerMask enemyMask;
   [SerializeField] private GameObject muzzleFlashPrefab;
   [SerializeField] private GameObject bloodPrefab;


   private LootData lootTemp;
   
   public Animator Animator;
   private AnimatorStateInfo stateInfo;
   private int animStateHash;
   
   private float timeSinceLastShot;
   private float timeSinceLastReload;
   private Rigidbody playerRb;
   
//Camera
    [SerializeField] private CameraShaker shaker;
    private float timerForRecoil;

   public delegate IEnumerator reload();

   private reload reloadFunc;
 

  

 //HUD
 [SerializeField] private TextMeshProUGUI ammoref;


    void OnEnable()
    {

          

   Animator.Rebind();
   firePoint = GameObject.Find("firePoint");
   audioSource.clip = gundata.equipClip;
   audioSource.Play();
    }


    private void Start()
    {
     

        firePoint = GameObject.Find("firePoint");
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = gundata.shotClip;
        gundata.isRelaoding = false;
        GlobalFuncs.isPlayerReloading = false;
    }

 
    bool CanReload() => !gundata.isRelaoding   && timeSinceLastReload > 2 && gundata.currentAmmoInMag < gundata.magazineCapacity && gundata.currentAmmo > 0 && animStateHash == Animator.StringToHash("Base Layer.Idle")
                        || animStateHash == Animator.StringToHash("Base Layer.Walking") ;
    bool CanShoot() => !gundata.isRelaoding && timeSinceLastShot > gundata.timeBetweenShot && animStateHash != Animator.StringToHash("Base Layer.Sprinting") && animStateHash != Animator.StringToHash("Base Layer.Equip") && gundata.currentAmmoInMag > 0  ;
 void Shoot()
 {
     audioSource.clip = gundata.shotClip;
      timeSinceLastShot = 0;
     gundata.currentAmmoInMag-= 1;

      audioSource.Play();



          //  for (int i = 0; i < gundata.VFX.Length; i++)
      {
      //    GameObject temp =  Instantiate(gundata.VFX[i], firePoint.transform.position , firePoint.transform.rotation);
     //     temp.layer = 8;
     //     Destroy(temp,5f);
      }
   


     

      RaycastHit hit;
      if (Physics.Raycast(crosshair.transform.position, playerCam.transform.forward, out hit, 30f, enemyMask))
      {
          GameObject bloodInstance = Instantiate(bloodPrefab,new Vector3(hit.transform.position.x,hit.transform.position.y + 1,hit.transform.position.z), Quaternion.Euler(Vector3.zero));
          Destroy(bloodInstance,10f);
         
          var enemyRef = hit.collider.gameObject.GetComponent<EnemyAI>();
          Debug.Log(enemyRef);
          enemyRef.HP -= gundata.damage;
          enemyRef.OnHit();
      }
    
      

 }



    IEnumerator Reload()
    {
    
    
          
                GlobalFuncs.isPlayerReloading = true;
                audioSource.clip = gundata.reloadClip;
                gundata.isRelaoding = true;
                timeSinceLastReload  = 0;
                audioSource.Play();
                Animator.SetTrigger("Reload");
                int temp;
                temp = Math.Clamp(lootTemp.quantity,0,gundata.ammoRelaodAmount);

                lootTemp.quantity -= temp;

                gundata.currentAmmoInMag = temp;
        
                yield return new WaitForSeconds(2f);
                gundata.isRelaoding = false;
                GlobalFuncs.isPlayerReloading = false;
         
                audioSource.clip = gundata.shotClip;

            
           

    }
    

   
    private void Update()
    {

       
      timeSinceLastShot += Time.deltaTime;
      timeSinceLastReload += Time.deltaTime;

    
      stateInfo = Animator.GetCurrentAnimatorStateInfo(0);
      animStateHash = stateInfo.fullPathHash;
      

      if (Input.GetKeyDown(KeyCode.R) && CanReload())
      {
          StartCoroutine(Reload());
        
      }

      if (gundata.isSemi)
      {
          if (Input.GetKeyDown(KeyCode.Mouse0) && animStateHash != Animator.StringToHash("Base Layer.Sprinting")  && CanShoot())
          {
            Animator.SetTrigger("Shoot");
              shaker.CallRecoilRSH12();
              Shoot();
            
          }
      }

      if (!gundata.isSemi)
      {
          if (CanShoot())
          {
              if (Input.GetKey(KeyCode.Mouse0))
              {
              
                  gundata.isShooting = true;
                  Shoot();
            
                  GlobalFuncs.isPlayerShooting = true;
              }
              else
              {
              
                  GlobalFuncs.isPlayerShooting = false;
                  gundata.isShooting = false;
              }
          }

          if (Input.GetKey(KeyCode.Mouse0) && gundata.currentAmmoInMag > 0 )
          {
              timerForRecoil += Time.fixedDeltaTime;
            
          }
          else
          {
              //shaker.CallResetCam();
              timerForRecoil = 0;
          }

          if (gundata.currentAmmoInMag < 1) GlobalFuncs.isPlayerShooting = false;
          

          if (gundata.currentAmmoInMag == 0)
          {
              if (Input.GetKeyDown(KeyCode.Mouse0))
              {
                  audioSource.clip = gundata.emptyClip;
                  audioSource.Play();
              }
            
          }
      }
       

        

        if (Mathf.Abs(GlobalFuncs.playerXInput) > 0.5||Mathf.Abs(GlobalFuncs.playerZInput) > 0.5)
        {
            Animator.SetBool("walking",true);
        }
        else
        {
            Animator.SetBool("walking",false);
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            Animator.SetBool("running",true);
        }
        else
        {
            Animator.SetBool("running",false);
            
        }
  
    
     
    }


   


 


   
    
}
