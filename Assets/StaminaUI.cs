using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaUI : MonoBehaviour
{
    [SerializeField] private Image staminaUsage;
    [SerializeField] private Image staminaBackground;

    [SerializeField] private PlayerControllee playerScriptRef;


    private Color backgrColor;
    private Color fillColor;
    // Start is called before the first frame update
    void Start()
    {
        backgrColor = staminaBackground.color;
        fillColor = staminaUsage.color;
        fillColor.a = 0;
        backgrColor.a = 0;
        
    }

    // Update is called once per frame
    void Update()
    {
        //   float staminaAmount = Mathf.Clamp(playerScriptRef.staminaAmount / 25, 0, 1.45f);
      //  staminaUsage.rectTransform.localScale = new Vector3(staminaAmount, 1, 1);

     
        if (!GlobalFuncs.isPlayerRunning)
        {

            StartCoroutine(TurnOff());
        }
        else
        {
            StartCoroutine(TurnOn());
        }
    }


    IEnumerator TurnOff()
    {
        yield return new WaitForSeconds(5f);
     
        float timer = 0;
        while (timer < 1.5f)
        {
            fillColor.a = Mathf.Lerp(fillColor.a, 0.2f, Time.deltaTime * 2);
            backgrColor.a = Mathf.Lerp(backgrColor.a, 0.2f, Time.deltaTime * 2);
            staminaBackground.color = backgrColor;
            staminaUsage.color = fillColor;
            timer += Time.deltaTime;
            if (GlobalFuncs.isPlayerRunning)
            {
                yield break;
            }
            yield return null;
            
        }
    }


    IEnumerator TurnOn()
    {
        float timer = 0;
        while (timer < 1.5f)
        {
            fillColor.a = Mathf.Lerp(fillColor.a, 0.7f, Time.deltaTime * 2);
           backgrColor.a = Mathf.Lerp(backgrColor.a, 0.7f, Time.deltaTime * 2);
           staminaBackground.color = backgrColor;
           staminaUsage.color = fillColor;
            timer += Time.deltaTime;
            yield return null;

        }
    }
    
    
    
}
