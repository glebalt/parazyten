using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RadioGame1 : MonoBehaviour
{
    // Start is called before the first frame update
    //refs
    [SerializeField] private Timer_Radio1 timerObj;
    [SerializeField] private Button_Radio startButton;
    [SerializeField] private GameObject timerUi;
    [SerializeField] private EnemySpawner_minigame1 spawnerRef;

    [SerializeField] private GameObject Crosshair;
    [SerializeField] private MiniGamePlayerStats_Radio1 miniGamePlayerMainRef;
    [SerializeField] private GameObject enemyMinigame;
    [SerializeField] private Scroll scrollRadio;
    [SerializeField] private LayerMask enemyMinigameMask;
    [SerializeField] private Transform[] miniGameSpawnPositions;

    private int counterForSpawn;
    
    private float xRotRef;
    private Vector3 targetPos;
    
    
    //GameStates
    public  enum GameStates
    {
        Started,
        Lost
    }

    

    private static GameStates currentGameState;

    public static GameStates GetMinigameState()
    {
        return currentGameState;
    }
    void Start()
    {
        currentGameState = GameStates.Lost;
        spawnerRef.enabled = false;
        counterForSpawn = 0;
        timerUi.SetActive(true);
        scrollRadio.GetComponent<Scroll>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (startButton.buttonPressed && currentGameState == GameStates.Lost)
        {
            currentGameState = GameStates.Started;
            StartCoroutine(InitGame());
        }
        xRotRef = scrollRadio.GetRotX();
        
        ChangePos();
        FindPos();
        if (CheckPlayerHpState())
        {
            currentGameState = GameStates.Lost;
            GameLost();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            RaycastHit hit;
      bool hitted  = Physics.Raycast(miniGamePlayerMainRef.transform.position, miniGamePlayerMainRef.transform.forward,out hit,enemyMinigameMask);
      if (hitted)
      {
          EnemyMinigameAI en = hit.transform.GetComponent<EnemyMinigameAI>();
          en.agentHP = en.agentHP - 100;
      }
           Debug.DrawRay(miniGamePlayerMainRef.transform.position, miniGamePlayerMainRef.transform.forward * 10,Color.blue);
        }
 
 
    }


    void ChangePos()
    {
        Crosshair.transform.localPosition = Vector3.Lerp(Crosshair.transform.localPosition, targetPos, Time.deltaTime * 4);
        Quaternion targetRot = Quaternion.Euler(miniGamePlayerMainRef.transform.localRotation.x,xRotRef,     miniGamePlayerMainRef.transform.localRotation.z);
        miniGamePlayerMainRef.transform.localRotation = Quaternion.Lerp(miniGamePlayerMainRef.transform.localRotation,targetRot,Time.deltaTime * 4 );
    }


    void FindPos()
    {
        if (Mathf.Abs(xRotRef) > 37)
        {
         targetPos = new Vector3(Crosshair.transform.localPosition.x, Crosshair.transform.localPosition.y, 3);
        
        
        }
        else
        {
            targetPos = new Vector3(Crosshair.transform.localPosition.x, Crosshair.transform.localPosition.y, 5);
        }
    }


   IEnumerator InitGame()
    {
        MiniGamePlayerStats_Radio1.playerHP = 100;
        spawnerRef.ClearEnemies();
  timerObj.StartTimerMethod();
       yield return new WaitForSeconds(5);
       timerUi.SetActive(false);
       scrollRadio.GetComponent<Scroll>().enabled = true;

       spawnerRef.enabled = true;
    }


   
    void GameLost()
    {
        scrollRadio.GetComponent<Scroll>().enabled = false;
        timerUi.SetActive(true);
        spawnerRef.enabled = false;
    }


    private bool CheckPlayerHpState()
    {
        if (MiniGamePlayerStats_Radio1.playerHP < 1)
        {
            return true;
        }

        return false;
    }
}
