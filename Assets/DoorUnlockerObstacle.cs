using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorUnlockerObstacle : MonoBehaviour
{
    private GameObject game;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    
 

    // Update is called once per frame
    void Update()
    {
        game = GameObject.Find("DoorUnlockerGame");
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player_DoorUnlocker"))
        {
       Vector3 targetImpulseVector = game.GetComponent<DoorUnlockGame>().playerMovementVector;
            other.gameObject.GetComponent<Rigidbody>().AddForce( -targetImpulseVector * 4,ForceMode.Impulse);
        }   
    }
}
