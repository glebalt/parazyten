using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMinigameAI : MonoBehaviour
{
    // Start is called before the first frame update
  public MiniGamePlayerStats_Radio1 minigamePlayer;
    private NavMeshAgent agent;
 public float agentHP;
    private float timerForAttack;
    void Start()
    {
        agentHP = 100;
        agent = GetComponent<NavMeshAgent>();
        timerForAttack = 0;
    }

    // Update is called once per frame
    void Update()
    {
        agent.SetDestination(minigamePlayer.transform.position);
        if (Vector3.Distance(minigamePlayer.transform.position, agent.transform.position) < 1.5f)
        {
            timerForAttack += Time.deltaTime;
            agent.isStopped = true;
            if (timerForAttack > 0.5f)
            {
                timerForAttack = 0;
                MiniGamePlayerStats_Radio1.playerHP = MiniGamePlayerStats_Radio1.playerHP - 10;

            }
           
        }


        if (agentHP < 1)
        {
            Destroy(gameObject);
        }
    }


}
