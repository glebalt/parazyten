using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.ProBuilder;
using Random = UnityEngine.Random;


public class DoorUnlockGame : MonoBehaviour
{
    [SerializeField] private GameObject targetObj;

   [SerializeField] private Material obstacleMaterial;

   public Vector3 playerMovementVector;
    private ProBuilderMesh mesh;

    public GameObject prefab;
    [SerializeField] private GameObject arrowPrefab;

    private IList<Face> faces;
    private IList<Face> facesForCheckpoints;

    private List<Vector3> spawnPositions = new List<Vector3>();
    private List<Vector3> usedPositions = new List<Vector3>();
    
    private List<Vector3> checkpointUsedPositions = new List<Vector3>();
    
    private List<GameObject> spawnedObjects = new List<GameObject>();
    private List<GameObject> spawnedCheckpoints = new List<GameObject>();

    
    //Timer

    [SerializeField] private TextMeshProUGUI timerTillEndRef;
    
    
    //Materials
    [SerializeField] private Material screenOnMaterail;
    [SerializeField] private Material screenOffMaterail;
    
    //PlayerInput
    private float inpX;
    private float inpZ;

    private Rigidbody playerRb;
    [SerializeField] private GameObject player;
    [SerializeField] private Camera minigameCamera;


    private GameObject playerIns;
    
    //Checkpoint
    [SerializeField] private GameObject checkpointPrefab;
    private bool firstCheckpointSpawned = false;
    private GameObject checkpointIns;
    private GameObject checkpointIns2;
 
    
    //Doors
    [SerializeField] private DoorSplittedController doorToOpen;

   public static int counterForCheckpoints;
   
   //Audio
   private AudioSource playerAudio;
   private AudioSource gameAudioPlayer;
   [SerializeField] private AudioClip playerMovementAudioClip;
   [SerializeField] private AudioClip gameLostAudioClip;
   [SerializeField] private AudioClip gameCompleteAudioClip;

    //Coroutines
    private Coroutine counter_Coroutine;
    // Start is called before the first frame update
    void Start()
    {
        gameAudioPlayer = GetComponent<AudioSource>();
        counterForCheckpoints = 0;
        spawnPositions.Clear();


     
       
        mesh = targetObj.GetComponent<ProBuilderMesh>();
        faces = new List<Face>();
        faces = mesh.faces;

        //CheckpointsPositions
        facesForCheckpoints = new List<Face>();
        facesForCheckpoints.Add(faces[94]);
        facesForCheckpoints.Add(faces[1012]);
        facesForCheckpoints.Add(faces[328]);
        facesForCheckpoints.Add(faces[482]);
        facesForCheckpoints.Add(faces[729]);


    


    }


    private void FixedUpdate()
    {
        if (InteractionScript.currentButton.gameStater == Button_Radio.GameStater.Started)
        {
          
            MinigamePlayerMovement_DoorUnlocker();
            try
            {
                arrowPrefab.transform.LookAt(checkpointIns.transform);
            }
            catch (MissingReferenceException e)
            {
                try
                {
                    arrowPrefab.transform.LookAt(checkpointIns2.transform);
                }
                catch (MissingReferenceException exception)
                {
                   
                }
              
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
       if (InteractionScript.currentButton.buttonPressed && InteractionScript.currentButton.gameStater == Button_Radio.GameStater.Lost)
       {
           doorToOpen = InteractionScript.currentButton.doorReference;
            StartCoroutine(GameInit());
            InteractionScript.currentButton.gameStater = Button_Radio.GameStater.Started;
       }
        
       
        
        if (InteractionScript.currentButton.gameStater == Button_Radio.GameStater.Finished)
        {
        
            doorToOpen.CallDoorUnlock();
        }
 
     
    }


    IEnumerator GameInit()
    {
       
        yield return new WaitForSeconds(2f);
        timerTillEndRef.transform.gameObject.SetActive(true);
     counter_Coroutine =  StartCoroutine(CounterTillGameLost());
        Debug.Log("SUCCESS");
        GeneratePattern();
      playerIns = Instantiate(player, new Vector3(0,0,0), quaternion.Euler(0, 0, 0));
        playerIns.transform.parent = targetObj.transform;
        playerIns.transform.localPosition = FindFaceCenter(faces[0]);
        playerRb = playerIns.GetComponent<Rigidbody>();
        playerAudio = playerIns.GetComponent<AudioSource>();
        playerAudio.clip = playerMovementAudioClip;
   
        
        
        
        
   checkpointIns = Instantiate(checkpointPrefab, new Vector3(0,0,0), quaternion.Euler(0, 0, 0));
      checkpointIns.transform.parent = targetObj.transform;
        checkpointIns.transform.localPosition = FindFaceCenter(facesForCheckpoints[Random.Range(1,4)]);
        
      spawnedObjects.Add(checkpointIns);
        
        while (InteractionScript.currentButton.gameStater == Button_Radio.GameStater.Started)
        {
          
            if (counterForCheckpoints == 1 && firstCheckpointSpawned == false)
            {
                
                firstCheckpointSpawned = true;
               checkpointIns2 = Instantiate(checkpointPrefab, new Vector3(0,0,0), quaternion.Euler(0, 0, 0));
               checkpointIns2.transform.parent = targetObj.transform;
                checkpointIns2.transform.localPosition = FindFaceCenter(facesForCheckpoints[Random.Range(1,4)]);
                spawnedObjects.Add(checkpointIns2);
               
             
            }
            if (counterForCheckpoints == 2)
            {
              
                InteractionScript.currentButton.gameStater = Button_Radio.GameStater.Finished;
                GameWon();
             
            }
      
          
            yield return null;
        }
     
    }


    void GameWon()
    {
        foreach (var VARIABLE in spawnedObjects)
        {
            Destroy(VARIABLE);
        }

            Destroy(playerIns);
        firstCheckpointSpawned = false;
        counterForCheckpoints = 0;
        StopCoroutine(GameInit());
        StopCoroutine(counter_Coroutine);
        counter_Coroutine = null;
        gameAudioPlayer.Stop();
        gameAudioPlayer.clip = gameCompleteAudioClip;
        gameAudioPlayer.Play();
        timerTillEndRef.transform.gameObject.SetActive(false);
    }


    void GameLost()
    {

        foreach (var VARIABLE in spawnedObjects)
        {
            Destroy(VARIABLE);
        }
        Destroy(playerIns);
        InteractionScript.currentButton.gameStater = Button_Radio.GameStater.Lost;
        timerTillEndRef.transform.gameObject.SetActive(false);
       gameAudioPlayer.Stop();
       gameAudioPlayer.clip = gameLostAudioClip;
        gameAudioPlayer.Play();
    }

    IEnumerator CounterTillGameLost()
    {
        int counterTillEnd = 30;
        yield return new WaitForSeconds(2f);

        while (counterTillEnd > 0)
        {
            counterTillEnd--;
            timerTillEndRef.text = counterTillEnd.ToString();
            yield return new WaitForSeconds(1);
        }
            
        GameLost();
        
    }

    Vector3 FindFaceCenter(Face face)
    {
        var vertexPos = mesh.positions;

        Vector3 center = Vector3.zero;
        
        foreach (int index in face.distinctIndexes)
        {
            center.x += vertexPos[index].x;
            center.y += vertexPos[index].y;
            center.z += vertexPos[index].z;
        }

        center /= face.distinctIndexes.Count;

        return center;
    }


    void GeneratePattern()
    {
   
        
        
        for (int i = 10; i < faces.Count; i++)
        {
            spawnPositions.Add(FindFaceCenter(faces[i]));
       
        }

        ClearObstacles();
        
        for (int i = 0; i < 300; i++)
        {
         
            Vector3 spawnPos = spawnPositions[Random.Range(0, 1000)];

            if (!usedPositions.Contains(spawnPos))
            {
                GameObject gmb = Instantiate(prefab, new Vector3(0,0,0), Quaternion.Euler(0, 0, 0));
              spawnedObjects.Add(gmb);
                gmb.transform.parent = targetObj.transform;
                gmb.transform.localPosition = spawnPos;
                usedPositions.Add(spawnPos);
            }
            
            
        
        }

    }

    void ClearObstacles()
    {
     
        SetUsedPositions();
        
        foreach (var obstacle in spawnedObjects)
        {
            Destroy(obstacle);
        }
    }


    void MinigamePlayerMovement_DoorUnlocker()
    {
        minigameCamera.transform.position = new Vector3(playerIns.transform.position.x,
            minigameCamera.transform.position.y, playerIns.transform.position.z);
        inpX = Input.GetAxisRaw("Horizontal");
        inpZ = Input.GetAxisRaw("Vertical");
       
     
        Vector3 dir = new Vector3(inpX, 0, inpZ);
        playerMovementVector = dir;
        playerRb.AddForce(dir * 15,ForceMode.Force);

        if (playerRb.velocity.magnitude > 0)
        {
            playerAudio.Play();
        }
        else
        {
            playerAudio.Pause();
        }
    }

    void SetUsedPositions()
    {
        usedPositions.Clear();
        for (int i = 0; i < 10; i++)
        {
            usedPositions.Add(spawnPositions[i]);
        }
        for (int i = 88; i < 100; i++)
        {
            usedPositions.Add(spawnPositions[i]);
        }
        usedPositions.Add(spawnPositions[1009]);
        usedPositions.Add(spawnPositions[1012]);
        usedPositions.Add(spawnPositions[526]);
        usedPositions.Add(spawnPositions[523]);
        usedPositions.Add(spawnPositions[1006]);
        usedPositions.Add(spawnPositions[1011]);
        
        for (int i = 332; i < 340; i++)
        {
            usedPositions.Add(spawnPositions[i]);
        }
        
        for (int i = 470; i < 493; i++)
        {
            usedPositions.Add(spawnPositions[i]);
        }
        
        for (int i = 726; i < 748; i++)
        {
            usedPositions.Add(spawnPositions[i]);
        }
        
    }
}
