using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsInDoorZone : MonoBehaviour
{
    // Start is called before the first frame update

    public bool IsInDoorArea;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            IsInDoorArea = true;
        }
        else
        {
            IsInDoorArea = false;
        }
    }
}
