using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSplittedController : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private GameObject leftDoor;
        [SerializeField] private GameObject rightDoor;
        private AudioSource doorAudioCon;

        [SerializeField]
       AudioClip doorOpenAudioClip;

       private bool soundPlayed;
        
    void Start()
    {
        soundPlayed = false;
        doorAudioCon = GetComponent<AudioSource>();
        doorAudioCon.clip = doorOpenAudioClip;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            CallDoorUnlock();
        }
    }

   IEnumerator DoorSplitUnlock()
    {
        
        Vector3 leftTargetPos = new Vector3(leftDoor.transform.localPosition.x + 0.8f, leftDoor.transform.localPosition.y,
            leftDoor.transform.localPosition.z);
        Vector3 rightTargetPos = new Vector3(rightDoor.transform.localPosition.x - 0.8f, rightDoor.transform.localPosition.y,
           rightDoor.transform.localPosition.z);
        float timer = 0;
        while (timer < 2f)
        {
            timer += Time.deltaTime;
            leftDoor.transform.localPosition = Vector3.Lerp(leftDoor.transform.localPosition, leftTargetPos, Time.deltaTime /2  );
          rightDoor.transform.localPosition = Vector3.Lerp(rightDoor.transform.localPosition, rightTargetPos, Time.deltaTime /2  );
            yield return null;
        }
       
    }

    public void CallDoorUnlock()
    {
        if (!soundPlayed)
        {
            doorAudioCon.volume = 0.5f;
            doorAudioCon.Play();
            StartCoroutine(DoorSplitUnlock());
            soundPlayed = true;
        }
     
    
    }
}
