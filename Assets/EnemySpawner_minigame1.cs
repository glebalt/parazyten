using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner_minigame1 : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private GameObject enemyMinigame;
    [SerializeField] private Transform[] miniGameSpawnPositions;
    [SerializeField] private MiniGamePlayerStats_Radio1 miniGamePlayerMainRef;
    private List<GameObject> enemyList = new List<GameObject>();
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
   
    }

    private void OnDisable()
    {
        CancelInvoke("SpawnEnemy");
    }

    private void OnEnable()
    {
        InvokeRepeating("SpawnEnemy",0,Random.Range(2,5));
    }

  


   public  void SpawnEnemy()
    {
        GameObject enemy = Instantiate(enemyMinigame,miniGameSpawnPositions[Random.Range(0,3)]);
        enemy.GetComponent<EnemyMinigameAI>().minigamePlayer = miniGamePlayerMainRef;
        enemy.layer = 12;
        enemyList.Add(enemy);
    }

    public void ClearEnemies()
    {
        foreach (var Enemy in enemyList)
        {
            Destroy(Enemy);
        }
    }
}
