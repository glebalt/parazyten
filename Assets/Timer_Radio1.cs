using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Timer_Radio1 : MonoBehaviour
{
    private TextMeshProUGUI timerText;
    // Start is called before the first frame update
    void Start()
    {
        timerText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


  private  IEnumerator StartTimer()
    {
        float timer = 0;
        float elapsedtime = 1f;
        float initFontSize = timerText.fontSize;
        while (timer < elapsedtime)
        {
            timer += Time.deltaTime;
            timerText.fontSize = Mathf.Lerp(timerText.fontSize, 30, Time.deltaTime * 4);
            timerText.text = "5";
            yield return null;
        }

        timer = 0;
        while (timer < elapsedtime)
        {
            timer += Time.deltaTime;
            timerText.fontSize = Mathf.Lerp(timerText.fontSize, initFontSize, Time.deltaTime * 4);
            timerText.text = "4";
            yield return null;
        }
        timer = 0;
        while (timer < elapsedtime)
        {
            timer += Time.deltaTime;
            timerText.fontSize = Mathf.Lerp(timerText.fontSize, initFontSize, Time.deltaTime * 4);
            timerText.text = "3";
            yield return null;
        }
        timer = 0;
        while (timer < elapsedtime)
        {
            timer += Time.deltaTime;
            timerText.fontSize = Mathf.Lerp(timerText.fontSize, initFontSize, Time.deltaTime * 4);
            timerText.text = "2";
            yield return null;
        }
        timer = 0;
        while (timer < elapsedtime)
        {
            timer += Time.deltaTime;
            timerText.fontSize = Mathf.Lerp(timerText.fontSize, initFontSize, Time.deltaTime * 4);
            timerText.text = "1";
            yield return null;
        }
        
    }

    public void StartTimerMethod()
    {
        StartCoroutine(StartTimer());
    }
}
