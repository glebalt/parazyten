using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class GetZBuffer : MonoBehaviour
{
    // Start is called before the first frame update

    private VisualEffect vfx;

    [SerializeField]
    private Transform ZBufferCamera;
    void Start()
    {
        vfx = GetComponent<VisualEffect>();
    }

    // Update is called once per frame
    void Update()
    {
        vfx.SetVector3("ZBuffer",ZBufferCamera.transform.position);
    }
}
